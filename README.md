# OpenAPI 3.0 Parser
This package provides functionality to analyze [OpenAPI 3.0 Specification] files
and build clients and servers from that specification.

For more details, please refer the [Project Documentation]

### Example Usage
From the command line:
```bash
$ python -m openapi_parser data/petstore.yaml 
```

From the Python:
```python
from openapi_parser.exporter import PackageWriter
from openapi_parser.parser.loader import OpenApiParser

def main():
    parser = OpenApiParser.open('data/petstore.yaml')
    parser.load_all()
    
    package_writer = PackageWriter(parser)
    package_writer.write_package()
    
    return 0

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
```

### TODOs:
 - [x] Model generator:
    - [x] Core functionality
    - [x] Class inheritance support
    - [x] Enums support
    - [ ] Required properties in combination with `readOnly`/`writeOnly`
    - [x] Discriminator property support
    - [ ] Cyclic refs resolution
    - [ ] Support of `additionalProperties` inside class
 - [x] Client generator
    - [x] Core functionality
    - [x] Smart body encoding
    - [x] Smart response decoding
    - [ ] `style`-encoding
    - [ ] `schema` and `content` inside parameters conflict support
    - [x] Enums for parameters support
    - [ ] Cookies parameters support
    - [x] Methods descriptions
    - [ ] Authorization support
 - [x] Metadata extractor
    - [x] Version
    - [x] Licence
    - [x] Description
    - [x] Servers
    - [x] Security definitions
 - [ ] Server generator
 - [ ] Test coverage
 - [ ] Documentation
 - [x] Command-line interface
    - [x] Basic CLI support
    - [ ] Extended CLI support

### See Also:
 - [Project Documentation]
 - [OpenAPI 3.0 Specification]
 - [JSON Schema Standard]

[Project Documentation]: https://hares-lab.gitlab.io/openapi-parser
[OpenAPI 3.0 Specification]: https://swagger.io/specification
[JSON Schema Standard]: https://tools.ietf.org/html/draft-wright-json-schema-validation-00
